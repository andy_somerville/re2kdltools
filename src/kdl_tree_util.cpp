/*
 * kdl_tree_util.cpp
 *
 *  Created on: Mar 4, 2013
 *      Author: andrew.somerville
 */

#include <re2/eigen/eigen_util.h>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <kdl/tree.hpp>
#include <eigen_conversions/eigen_kdl.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <iostream>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <kdl_parser/kdl_parser.hpp>



Eigen::Vector4d  aggrigatePointMasses( const Eigen::Vector4d & pm0, const Eigen::Vector4d pm1 )
{
    Eigen::Vector4d resultPointMass(0,0,0,0);

    double totalMass = pm0(3) + pm1(3);

    if( totalMass == 0 )
        return resultPointMass;

    double v0factor  = pm0(3) / totalMass;
    double v1factor  = pm1(3) / totalMass;

    resultPointMass(3)        = totalMass;
    resultPointMass.head<3>() = (pm0.head<3>() * v0factor) + (pm1.head<3>() * v1factor);

    return resultPointMass;
}





void printTree( const KDL::TreeElement & element, const KDL::Tree & tree, int depth )
{
    std::string indent( depth, ' ' );

    const KDL::Segment &  segment    = element.segment;
    const KDL::Joint   &  joint      = segment.getJoint();
    Eigen::Vector3d       jointAxis  = Eigen::Map< Eigen::Vector3d >( joint.JointAxis().data) ;
    int                   jointIndex = element.q_nr;


//    if( joint.getType() != KDL::Joint::None )
    {
        std::cout << indent << "Index:        " << jointIndex << " \n";
        std::cout << indent << "Element ptr:  " << &element << " \n";
        if( element.segment.getName() != tree.getRootSegment()->first )
            std::cout << indent << "Parent:  " << element.parent->first << "\n";
        else
            std::cout << indent << "Parent:  [noParent]" << "\n";
        std::cout << indent << "Segment: " << segment.getName() << "\n";
        std::cout << indent << "  pose:       " << Eigen::Vector3dMap(segment.pose(0).p.data).transpose()           << "\n";
        std::cout << indent << "  framtToTip: " << Eigen::Vector3dMap(segment.getFrameToTip().p.data    ).transpose() << "\n";
        std::cout << indent << "  CoG:        " << Eigen::Vector3dMap(segment.getInertia().getCOG().data).transpose()  << "\n";
        std::cout << indent << "  Mass:       " << segment.getInertia().getMass() << "\n";
        std::cout << indent << "Joint: " << joint.getName()     << "\n";
        std::cout << indent << "  axis:  " << Eigen::Vector3dMap( joint.JointAxis().data ).transpose() << "\n";
        std::cout << indent << "  offset:" << Eigen::Vector3dMap( joint.pose(0).p.data   ).transpose() << "\n";
        std::cout << indent << "  type:"   << joint.getType() << "\n";
        std::cout << indent << "\n";

        ++depth;
    }

    BOOST_FOREACH( const KDL::SegmentMap::const_iterator & entry, element.children )
    {
        printTree( entry->second, tree, depth );
    }
}


void printTree( const KDL::Tree & tree )
{
    printTree( tree.getRootSegment()->second, tree, 0 );
}



double calcTreeMass( const KDL::TreeElement & element )
{
    const KDL::Segment &  segment    = element.segment;

    double totalMass = 0;

//    if( element.children.size() > 1 )
//        ROS_INFO_STREAM( "Calc from: " << segment.getName() << " which has: " << element.children.size() << " children" );
    BOOST_FOREACH( const KDL::SegmentMap::const_iterator & entry, element.children )
    {
        double childMass = 0;
        childMass = calcTreeMass( entry->second );
//        if( element.children.size() > 1 )
//            ROS_WARN_STREAM( "in " << segment.getName() << " Total mass below " << entry->first << " should be " << childMass );
        totalMass += childMass;
    }

    totalMass += segment.getInertia().getMass();

//    if( element.children.size() > 1 )
//    ROS_INFO_STREAM( segment.getInertia().getMass() << " for " << element.q_nr << " which is " << segment.getName() );

    return totalMass;
}

double calcTreeMass( const KDL::Tree & tree )
{
    return calcTreeMass( tree.getRootSegment()->second );
}

Eigen::Affine3d
getTransform( KDL::TreeFkSolverPos_recursive & treeSolver, const std::string & baseFrame, const std::string & tipFrame, const Eigen::VectorXd & jointPositions )
{
    KDL::Frame baseToPelvis;
    KDL::Frame tipToPelvis;


    KDL::JntArray kdlJointPositions;
    kdlJointPositions.data = jointPositions; //FSCK YOU KDL! What is the point of this damn class!

    int errorVal = 0;
    if( errorVal >= 0 )
        errorVal = treeSolver.JntToCart( kdlJointPositions,  baseToPelvis, baseFrame );
    if( errorVal >= 0 )
        errorVal = treeSolver.JntToCart( kdlJointPositions,  tipToPelvis,  tipFrame  );


    if( errorVal != 0 )
    {
        std::cerr << "jtnToCart for " << baseFrame << " to " << tipFrame << " gave error code: " << errorVal << std::endl;
        std::cerr << "Jointpositions were: " << jointPositions.transpose() << std::endl;

        //FIXME this shouldn't just continue... need to throw excpetion or indicate failure somehow
        throw std::runtime_error( "Failed to get transform" );
    }


    KDL::Frame tipToBase = baseToPelvis.Inverse() * tipToPelvis;

    Eigen::Affine3d tipToBaseTransform( Eigen::Affine3d::Identity() );
    tf::transformKDLToEigen( tipToBase, tipToBaseTransform );

    return tipToBaseTransform;
}


template <typename MatrixT>
void transformEigenToKdl_template( const MatrixT &e, KDL::Frame &k)
{
    k.p[0]   = e(0,3);
    k.p[1]   = e(1,3);
    k.p[2]   = e(2,3);

    k.M(0,0) = e(0,0);
    k.M(0,1) = e(0,1);
    k.M(0,2) = e(0,2);
    k.M(1,0) = e(1,0);
    k.M(1,1) = e(1,1);
    k.M(1,2) = e(1,2);
    k.M(2,0) = e(2,0);
    k.M(2,1) = e(2,1);
    k.M(2,2) = e(2,2);
}

void transformEigenToKdl( const Eigen::Affine3d &e, KDL::Frame &k)
{
    transformEigenToKdl_template( e, k );
}

void transformEigenToKdl( const Eigen::Matrix4d &e, KDL::Frame &k)
{
    transformEigenToKdl_template( e, k );
}

namespace kdl_parser
{
    bool addChildrenToTree(boost::shared_ptr<const urdf::Link> root, KDL::Tree& tree);
}

bool kdlRootInertiaWorkaround(const urdf::Model& robotModel, KDL::Tree& tree)
{
    tree = KDL::Tree( "root" );

    boost::shared_ptr<urdf::Joint> rootJoint( new urdf::Joint );
    rootJoint->type =  urdf::Joint::FIXED;
    rootJoint->name =  "root_jnt";
    rootJoint->parent_link_name =  "root";

    if( robotModel.root_link_ == NULL )
    {
        std::cerr << "NO ROOT LINK, kdlRootInertiaWorkaround failing!\n";
        return false;
    }
    robotModel.root_link_->parent_joint = rootJoint;
    kdl_parser::addChildrenToTree( robotModel.getRoot(), tree );

//    tree.getRootSegment()->second.parent = tree.getSegments().end(); //Failed workaround

    //  add all children
    for( size_t i=0; i<robotModel.getRoot()->child_links.size(); i++ )
    {
        if( !kdl_parser::addChildrenToTree( robotModel.getRoot()->child_links[i], tree) )
            return false;
    }

    return true;
}

bool kdlRootInertiaWorkaround(const std::string& xml, KDL::Tree& tree)
{
    TiXmlDocument urdf_xml;
    urdf_xml.Parse(xml.c_str());
    return kdlRootInertiaWorkaround(&urdf_xml, tree);
}

bool kdlRootInertiaWorkaround(TiXmlDocument *xml_doc, KDL::Tree& tree)
{
    urdf::Model robot_model;
    if (!robot_model.initXml(xml_doc))
    {
        std::cerr << "Could not generate robot model" << std::endl;
        return false;
    }
    return kdlRootInertiaWorkaround(robot_model, tree);
}

const KDL::TreeElement & getTreeElement( const KDL::Tree & tree, const std::string & segmentName )
{
    KDL::SegmentMap::const_iterator entry;
    entry = tree.getSegment( segmentName );
    if( entry != tree.getSegments().end() )
        return entry->second;
    else
        throw std::runtime_error( std::string( "Missing segment!" ) + segmentName );
}


